import os
import re
import sys
import pathlib
sys.path.pop(0)
from setuptools import setup
sys.path.append("./sdist_upip")
import sdist_upip

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()


setup(
    name="micropython-firebase-realtime",
    version="0.0.1-1",
    author="mayankjohri",
    author_email="mayankjohri@gmail.com",
    description="Firebase realtime implementation for Micropython",
    url="https://gitlab.com/mayankjohri/micropython-firebase-realtime",
    long_description=README,
    long_description_content_type="text/markdown",
    packages=[""],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    keywords=[
        "firebase",
        "Google"
        "Microcontroller",
        "Micropython"
    ],
    cmdclass={'sdist': sdist_upip.sdist}
)
